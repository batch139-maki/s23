// insertOne Method
db.rooms.inserOne(
	{
		name: "single",
		accomodates: 2,
		price: 1000,
		description: "A simple room with all the basic necessities",
		rooms_available: 10,
		isAvailable: false
	}
);

// insertMany Method
db.rooms.insertMany(
	[
		{
			name: "queen",
			accommodates: 4,
			price: 4000,
			description: "A room with queen sized bed perfect for a simple getaway",
			rooms_available: 15,
			isAvailable: false
		},
		{
			name: "double",
			accommodates: 3,
			price: 2000,
			description: "A room for a small family going on a vacation",
			rooms_available: 5,
			isAvailable: false
		}

	]

);

// find Method
db.rooms.find(
	{

		name: "double"
	}
);

// updateOne Method

db.rooms.updateOne(
	{
		name: "queen"
	},
	{
		$set: {
			rooms_available: 0
		}
	}
);

// deleteMany Method

db.rooms.deleteMany(
	{
		rooms_available: 0
	}
);
